#!/usr/bin/env python3

# -------
# imports
# -------

from math import sqrt
import pickle
from requests import get
from os import path
from numpy import sqrt, square, mean, subtract

def create_cache(filename):
    """
    filename is the name of the cache file to load
    returns a dictionary after loading the file or pulling the file from the public_html page
    """
    cache = {}
    filePath = "/u/fares/public_html/netflix-caches/" + filename
    
    if path.isfile(filePath):
        with open(filePath, "rb") as f:
            cache = pickle.load(f)
    else:
        webAddress = "http://www.cs.utexas.edu/users/fares/netflix-caches/" + \
            filename
        bytes = get(webAddress).content
        cache = pickle.loads(bytes)

    return cache

avg_rating = 3.60428996442
actual_rating = create_cache("cache-actualCustomerRating.pickle")
avg_movie_rating_by_year = create_cache("cache-movieAverageByYear.pickle")
year_of_rating = create_cache("cache-yearCustomerRatedMovie.pickle")
avg_customer_rating_by_year = create_cache("cache-customerAverageRatingByYear.pickle")

# ------------
# netflix_eval
# ------------

def netflix_eval(reader, writer) :
    predictions = []
    actual = []
    # iterate throught the file reader line by line
    for line in reader:
        # need to get rid of the '\n' by the end of the line
        line = line.strip()
        # check if the line ends with a ":", i.e., it's a movie title 
        if line[-1] == ':':
	    # It's a movie
            current_movie = line.rstrip(':')
            writer.write(line)
            writer.write('\n')
        else:
	    # It's a customer
            current_customer = line
            # find year movie was rated
            yrcust = year_of_rating[int(current_customer), int(current_movie)]
            assert type(yrcust) == int
            # find the average rating of the customer for that year
            cmrating = avg_customer_rating_by_year[int(current_customer), yrcust]
            # find the average rating of the movie for that year
            mvrating = avg_movie_rating_by_year[int(current_movie), yrcust]
                
            # checking values
            assert type(mvrating) == float
            assert type(cmrating) == float
            assert 1.0 <= mvrating <= 5.0
            assert 1.0 <= cmrating <= 5.0
            
            # calculate predictions
            prediction = (round(mvrating + cmrating - avg_rating, 1))
           
            # make sure the prediction is between 1 and 5
            if prediction > 5.0:
                prediction = 5.0
            if prediction < 1.0:
                prediction = 1.0
            assert 1.0 <= prediction <= 5.0
            assert type(prediction) == float
            
            # create predictions and actuals
            predictions.append(prediction)
            actual.append(actual_rating[int(current_customer), int(current_movie)])
            writer.write(str(prediction))
            writer.write('\n')	

    # calculate rmse for predictions and actuals
    rmse = sqrt(mean(square(subtract(predictions, actual))))
    writer.write(str(rmse)[:4] + '\n')
    return rmse
